# Flask-Blog
Un blog creat in Python. Dezvoltare incipienta!

## Dependente
- [python >=3.6](https://www.python.org/)
- [flask](https://github.com/pallets/flask)
- [flask-wtf](https://github.com/lepture/flask-wtf)
- [flask-sqlalchemy](https://github.com/pallets/flask-sqlalchemy)

### Schreenshots
<p align="center">
  <img src="https://i.imgur.com/sVXZD5t.png" alt="home"/>
  <img src="https://i.imgur.com/5WK4PZy.png" alt="about"/>
  <img src="https://i.imgur.com/zdhrdwn.png" alt="login"/>
  <img src="https://i.imgur.com/bXTLqnT.png" alt="register"/>
  <img src="https://i.imgur.com/s2gx5uW.png" alt="create_user"/>
</p>
