import requests
import threading
import os
import time

class Download(threading.Thread):
    def __init__(self, url, path):
        threading.Thread.__init__(self)
        self.url = url
        self.path = path
        self.details = None
        self.__thread = True
        self.__delete_path()
        self.__get_headers()

    def __get_headers(self):
        try:
            r = requests.get(self.url, stream=True)
            if r.ok:
                self._start_thread()
                self.details = {
                    'status_code' : int(r.status_code),
                    # 'filename' : r.headers.get('Content-Disposition').split('="')[1][:-1],
                    # 'content' : r.headers.get('Content-Type'),
                    'length' : int(r.headers.get('Content-Length'))
                }
            else:
                self._kill_thread()
        except requests.ConnectionError:
            self._kill_thread()

    def __delete_path(self):
        if os.path.exists(self.path):
            os.remove(self.path)

    def __get_size_path(self):
        if os.path.exists(self.path):
            return os.path.getsize(self.path)
        return 0

    def __get_details(self, parameter):
        if self.details:
            return self.details.get(parameter)
        return None

    def _kill_thread(self):
        self.__thread = False

    def _start_thread(self):
        self.__thread = True

    def get_status_code(self):
        return self.__get_details('status_code')

    # def get_name(self):
    #     return self.__get_details('filename')
    
    # def get_content(self):
    #     return self.__get_details('content')

    def get_length(self):
        return self.__get_details('length')

    def get_progress(self):
        if self.get_status_code():
            return int(self.__get_size_path() / self.get_length() * 100)
        return 0

    def is_finish(self):
        return self.get_progress() == 100

    def run(self):
        if self.__thread:
            try:
                self.__get_headers()
                r = requests.get(self.url, stream=True)
                with open(self.path, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192): 
                        if chunk:
                            f.write(chunk)
            finally:
                self._kill_thread()
                # if not self.is_finish():
                #     self.__delete_path()

if __name__ == '__main__':
    d = Download('https://images.unsplash.com/photo-1548439935-5f5f0a4fb02e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=890&q=80', 'image.jpg')
    d.start()
 
